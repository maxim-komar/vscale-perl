package VScale::Log;
use warnings;
use strict;

our $VERSION = q{1.0.0};

use JSON::XS;

sub pretty_json
{
    my ( $message ) = @_;
    return JSON::XS->new->canonical(1)->encode( $message );
}

1;
