package VScale::HTTP;
use warnings;
use strict;

our $VERSION = q{1.0.0};

use feature qw( state );

use English qw( -no_match_vars );
use JSON::XS;
use LWP::UserAgent;
use Params::Validate qw( :all );

use VScale::Constant;
use VScale::Log;

sub http_agent
{
    my $ua_ref = LWP::UserAgent->new;
    return $ua_ref;
}

sub get
{
    state $spec_href = {
        url     => { type => SCALAR  },
        headers => { type => HASHREF },
    };
    my %arg = validate( @_, $spec_href );

    my $ua_ref = http_agent();

    my $resp_ref = $ua_ref->get( $arg{url},
        %{ $arg{headers} }
    );

    my $content = $resp_ref->decoded_content;

    if ( not $resp_ref->is_success )
    {
        return (
            $VScale::Constant::STATUS_ERROR,
            sprintf q{some error occured: %s, code: %s},
                $resp_ref->status_line, $resp_ref->code
        );
    }

    my $result_ref = eval {
        JSON::XS::decode_json( $content );
    };
    if ( $EVAL_ERROR )
    {
        return (
            $VScale::Constant::STATUS_ERROR,
            sprintf q{can't decode response: %s}, $content
        );
    }

    return (
        $VScale::Constant::STATUS_OK,
        $result_ref
    );
}

sub post
{
    state $spec_href = {
        url     => { type => SCALAR  },
        headers => { type => HASHREF },
        content => { type => SCALAR | HASHREF | ARRAYREF },
    };
    my %arg = validate( @_, $spec_href );

    my $ua_ref = http_agent();

    my $postdata;
    if (
           ( ref( $arg{content} ) eq q{HASH}  )
        || ( ref( $arg{content} ) eq q{ARRAY} )
    )
    {
        $postdata = JSON::XS::encode_json( $arg{content} );
    }
    else
    {
        $postdata = $arg{content};
    }

    my $resp_ref = $ua_ref->post( $arg{url},
        %{ $arg{headers} },
        Content => $postdata
    );

    my $content = $resp_ref->decoded_content;

    if ( not $resp_ref->is_success )
    {
        return (
            $VScale::Constant::STATUS_ERROR,
            sprintf q{some error occured: %s, code: %s, message: %s},
                $resp_ref->status_line, $resp_ref->code, $resp_ref->header( q{vscale-error-message} )
        );
    }

    my $result_ref = eval {
        JSON::XS::decode_json( $content );
    };
    if ( $EVAL_ERROR )
    {
        return (
            $VScale::Constant::STATUS_ERROR,
            sprintf q{can't decode response: %s}, $content
        );
    }

    return (
        $VScale::Constant::STATUS_OK,
        $result_ref
    );
}

sub delete
{
    state $spec_href = {
        url     => { type => SCALAR  },
        headers => { type => HASHREF },
    };
    my %arg = validate( @_, $spec_href );

    my $ua_ref = http_agent();

    my $resp_ref = $ua_ref->delete( $arg{url},
        %{ $arg{headers} }
    );

    my $content = $resp_ref->decoded_content;

    if ( not $resp_ref->is_success )
    {
        return (
            $VScale::Constant::STATUS_ERROR,
            sprintf q{some error occured: %s, code: %s},
                $resp_ref->status_line, $resp_ref->code
        );
    }

    my $result_ref = eval {
        JSON::XS::decode_json( $content );
    };
    if ( $EVAL_ERROR )
    {
        return (
            $VScale::Constant::STATUS_ERROR,
            sprintf q{can't decode response: %s}, $content
        );
    }

    return (
        $VScale::Constant::STATUS_OK,
        $result_ref
    );
}

1;
