package VScale::Constant;
use warnings;
use strict;

our $VERSION = q{1.0.0};

use Readonly;

{
    # API endpoints
    Readonly::Scalar our $INSTANCES_URL       => q{https://api.vscale.io/v1/scalets};
    Readonly::Scalar our $SINGLE_INSTANCE_URL => q{https://api.vscale.io/v1/scalets/:ctid};

    Readonly::Scalar our $SSH_KEYS_URL  => q{https://api.vscale.io/v1/sshkeys};
}

{
    Readonly::Scalar our $STATUS_OK    => q{OK};
    Readonly::Scalar our $STATUS_ERROR => q{ERROR};
}

{
    Readonly::Scalar our $OUTPUT_FORMAT_JSON => q{json};
}

1;
