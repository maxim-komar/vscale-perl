package VScale::SshKey;
use warnings;
use strict;

our $VERSION = q{1.0.0};

use feature qw( state );

use Params::Validate qw( :all );

use VScale::Constant;
use VScale::HTTP;

sub list
{
    state $spec_href = {
        token => { type => SCALAR },
    };
    my %arg = validate( @_, $spec_href );

    my ( $status, $response ) = VScale::HTTP::get(
        url     => $VScale::Constant::SSH_KEYS_URL,
        headers => {
            q{X-Token} => $arg{token},
        }
    );

    return ( $status, $response );
}

1;
