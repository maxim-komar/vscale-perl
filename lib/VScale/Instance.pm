package VScale::Instance;
use warnings;
use strict;

our $VERSION = q{1.0.0};

use feature qw( state );

use Params::Validate qw( :all );
use Readonly;

use VScale::Constant;
use VScale::HTTP;

sub create
{
    state $spec_href = {
        token     => { type => SCALAR   },
        make_from => { type => SCALAR   },
        rplan     => { type => SCALAR   },
        keys      => { type => ARRAYREF },
        name      => { type => SCALAR   },
        location  => { type => SCALAR   },
        do_start  => { type => SCALAR, default => 1 }
    };
    my %arg = validate( @_, $spec_href );

    my ( $status, $response ) = VScale::HTTP::post(
        url     => $VScale::Constant::INSTANCES_URL,
        headers => {
            q{Content-Type} => q{application/json;charset=UTF-8},
            q{X-Token}      => $arg{token},
        },
        content => {
            make_from => $arg{make_from},
            rplan     => $arg{rplan},
            keys      => $arg{keys},
            name      => $arg{name},
            location  => $arg{location},
            do_start  => ( $arg{do_start} ? \1 : \0 ),
        }
    );

    return ( $status, $response );
}

sub list
{
    state $spec_href = {
        token => { type => SCALAR }
    };
    my %arg = validate( @_, $spec_href );

    my ( $status, $response ) = VScale::HTTP::get(
        url     => $VScale::Constant::INSTANCES_URL,
        headers => {
            q{X-Token} => $arg{token},
        },
    );

    return ( $status, $response );
}

sub get
{
    state $spec_href = {
        token => { type => SCALAR },
        id    => { type => SCALAR },
    };
    my %arg = validate( @_, $spec_href );

    my ( $status, $response ) = VScale::HTTP::get(
        url     => get_instance_url( id => $arg{id} ),
        headers => {
            q{X-Token} => $arg{token},
        },
    );

    return ( $status, $response );
}

sub delete
{
    state $spec_href = {
        token => { type => SCALAR },
        id    => { type => SCALAR },
    };
    my %arg = validate( @_, $spec_href );

    my ( $status, $response ) = VScale::HTTP::delete(
        url     => get_instance_url( id => $arg{id} ),
        headers => {
            q{X-Token} => $arg{token},
        },
    );

    return ( $status, $response );
}
sub get_instance_url
{
    state $spec_href = {
        id => { type => SCALAR },
    };
    my %arg = validate( @_, $spec_href );

    my $url = $VScale::Constant::SINGLE_INSTANCE_URL;
    $url =~ s/:ctid/$arg{id}/g;

    return $url;
}

1;
